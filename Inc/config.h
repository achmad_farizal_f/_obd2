/**
 * \file    config.h
 * \brief   Global configuration file.
 *
 *  Created on: Sep 13, 2017
 *      Author: wisnu.pramadi
 */

#ifndef CONFIG_H_
#define CONFIG_H_


/**
 * @def    Nucleo Board
 * @brief  For use with nucleo board only, disable it if use pci proto board.
 * */
//#define USE_NUCLEO


/**
 * @def    DATA_VALID
 * @brief  Data is valid
 * */
#define DATA_VALID                  1

/**
 * @def    DATA_INVALID
 * @brief  Data is invalid
 * */
#define DATA_INVALID                0

/**
 * @def    DEVICE_UNIQUE_ID_ADDR
 * @brief  Unique ID's address
 * */
#define DEVICE_UNIQUE_ID_ADDR       0x1FFF7A10

/**
 * @def    DEVICE_UNIQUE_ID_LENGTH
 * @brief  Unique ID's byte length
 * */
#define DEVICE_UNIQUE_ID_LENGTH     12

/**
 * @def    FW_VERSION
 * @brief  Return the version string of current firmware
 * */
#define FW_VERSION                  "001.00C"

/**
 * @def    SECONDARY_FW_ADDR
 * @brief  Return the address for storing firmware upgrade
 * */
#define SECONDARY_FW_ADDR           0x08160000

/**
 * @def    SECONDARY_FW_SPACE
 * @brief  Return the available space for firmware upgrade
 * */
#define SECONDARY_FW_SPACE          0x08160000

/**
 * @def    ENABLE_WWDG
 * @brief  Enable watchdog
 * */
#define ENABLE_WWDG

/**
 * @def    ENABLE_DEBUG
 * @brief  Enable debug mode
 * */
#define ENABLE_DEBUG

/**
 * @def    USE_ACCELEROMETER
 * @brief  Use accelerometer
 * */
#define USE_ACCELEROMETER

/**
 * @def    USE_TRACKER
 * @brief  Use GPS
 * */
#define USE_TRACKER

/**
 * @def    USE_OBD
 * @brief  Use CAN bus for On Board Diagnostic
 * */
#define USE_OBD

/**
 * @def    USE_DFU
 * @brief  Use Device Firmware Upgrade, Not implemented for OBDIIV2.
 * */
#define USE_DFU

/**
 * @def    USE_CELLULAR
 * @brief  Use CELLULAR Modem Device
 * */
#define USE_CELLULAR

/**
 * @def    USE_BT
 * @brief  Activate Bluetooth Classic or Low Energy
 * */
#define USE_BT

/**
 * @def    USE_BLE_PAN
 * @brief  Use Bluetooth Low Energy instead of BT Classic.
 * */
#define USE_BLE_PAN

/**
 * @def    USE_VCP
 * @brief  Use Virtual Com Port for Debug
 * */
//#define USE_VCP

/**
 * @def    USE_OTG
 * @brief  Use USB OTG to copy data into usb drive.
 * */
#define USE_OTG

/**
 * @def    USE_NOR_FLASH
 * @brief  ENABLE NOR FLASH
 * */
#define USE_NOR_FLASH

/**
 * @def    USE_INT_EEPROM
 * @brief  Use STM32 Flash as EEPROM
 * need to be disable if not using bootloader
 * */
#define USE_INT_EEPROM

/**
 * @def    USE_INT_RTC
 * @brief  ENABLE Internal RTC
 * */
#define USE_INT_RTC

/**
 * @def    USE_ADC
 * @brief  ENABLE ADC
 * */
#define USE_ADC

/**
 * @def    ENABLE_VERBOSE_MESSAGE
 * @brief  Enable more messages in JSON format
 * */
//#define ENABLE_VERBOSE_MESSAGE

/**
 * @def    ENABLE_DELTA_ENCODING
 * @brief  Enable delta encoding for numeric array
 * */
//#define ENABLE_DELTA_ENCODING

/**
 * @def    ENABLE_PROFILER
 * @brief  Enable profiler to find out CPU usage
 * */
#define ENABLE_PROFILER

#ifdef ENABLE_DEBUG
#include <stdio.h>

/**
 * @def    DEBUG_PRINT
 * @brief  Alias for printf. Only enabled in DEBUG mode.
 * */
#define DEBUG_PRINT(...)            printf(__VA_ARGS__)

/**
 * @def    DEBUG_FAILED
 * @brief  Print current function name that failed. Only enabled in DEBUG mode.
 * */
#define DEBUG_FAILED()              DEBUG_PRINT(">>>Function failed: %s.\r\n", __FUNCTION__)
#else

/**
 * @def    DEBUG_PRINT
 * @brief  Alias for nothing.
 * */
#define DEBUG_PRINT(...)
#endif

#ifdef USE_TRACKER
/**
 * @def    SYS_MAX_LOCATION
 * @brief  Number of locations for geofencing
 * */
#define GEO_MAX_LOCATION        15
#endif

extern uint8_t unique_id[12];

#endif /* CONFIG_H_ */
