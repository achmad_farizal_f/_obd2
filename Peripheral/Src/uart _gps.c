/*
 * uart.c
 *
 *  Created on: 11 Nov 2018
 *      Author: Achmad Farizal
 */

#include "uart_gps.h"


/* #################### Internal macros #################### */
#define UART7_BAUDRATE              9600
#define UART7_BUFFER_SIZE           1536
#define UART7_BUFFER_COUNT          2

/* #################### Internal variables #################### */
static UART_HandleTypeDef 		_uart7_handle;              /**< UART7 handle */
static uint8_t 					_uart7_buffer[UART7_BUFFER_SIZE*UART7_BUFFER_COUNT]; /**< UART7 buffer instance */
static uint8_t 					_uart7_write_idx = 0;                                /**< UART7 buffer's write index */
static uint8_t 					_uart7_read_idx = 0;                                 /**< UART7 buffer's read index */



void Uart_gps_config(void){
/* Configure uart7 as TX & RX */
    _uart7_handle.Instance 		  = UART7;
    _uart7_handle.Init.BaudRate   = UART7_BAUDRATE;
    _uart7_handle.Init.WordLength = UART_WORDLENGTH_8B;
    _uart7_handle.Init.StopBits   = UART_STOPBITS_1;
    _uart7_handle.Init.Parity 	  = UART_PARITY_NONE;
    _uart7_handle.Init.Mode 	  = UART_MODE_TX_RX;
    _uart7_handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    _uart7_handle.Init.OverSampling = UART_OVERSAMPLING_16;

    if (HAL_UART_Init(&_uart7_handle) != HAL_OK)
    {
    	Error_Handler();
    }

    /* Enable receive */
	if(HAL_UART_Receive_DMA(&_uart7_handle, &_uart7_buffer[_uart7_write_idx*UART7_BUFFER_SIZE], UART7_BUFFER_SIZE) != HAL_OK)
	{
		Error_Handler();
	}

	/* Interrupt on UART idle */
	__HAL_UART_ENABLE_IT(&_uart7_handle, UART_IT_IDLE);

	/* Interrupt on DMA TX complete */
	__HAL_DMA_ENABLE_IT(&hdma_uart7_tx, DMA_IT_TC);
}



