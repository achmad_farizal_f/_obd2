#ifndef UARTDEBUG_H
#define UARTDEBUG_H

#include "main.h"
#include "stm32f7xx_hal.h"

void Uart_Debug_config(void);
uint8_t _uart3_send(char data);
//HAL_StatusTypeDef debug_send(uint8_t *txData);
uint8_t debug_send(void);
/**
 * @brief   UART callback typedef
 *
 * @param   data The data pointer
 * @param   length The data length
 * */
typedef void (*UARTPeriphCallback)(uint8_t *data, uint16_t length);

#endif
