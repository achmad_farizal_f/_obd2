#ifndef UART_H
#define UART_H

#include "main.h"
#include "uart_gps.h"
#include "uart_debug.h"

void Uart_Init(void);
void Uart_config(void);

#endif
