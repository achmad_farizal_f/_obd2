/*
 * gps.h
 *
 *  Created on: 11 Nov 2018
 *      Author: Achmad Farizal
 */
#ifndef GPS_H
#define GPS_H

#include "stm32f7xx.h"
#include "stm32f7xx_hal.h"
#include "main.h"

uint8_t gps_init(void);

#endif
