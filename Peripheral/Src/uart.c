/*
 * uart.c
 *
 *  Created on: 4 Nov 2019
 *      Author: Achmad Farizal
 */

#include "stm32f7xx.h"
#include "stm32f7xx_hal.h"
#include "uart.h"

void Uart_Init(void);

#define USARTx                           USART3
#define USARTx_CLK_ENABLE()              __HAL_RCC_USART3_CLK_ENABLE();
#define USARTx_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOC_CLK_ENABLE()
#define USARTx_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOC_CLK_ENABLE()

#define USARTx_FORCE_RESET()             __HAL_RCC_USART3_FORCE_RESET()
#define USARTx_RELEASE_RESET()           __HAL_RCC_USART3_RELEASE_RESET()

/* Definition for USARTx Pins */
#define USARTx_TX_PIN                    GPIO_PIN_10
#define USARTx_TX_GPIO_PORT              GPIOC
#define USARTx_TX_AF                     GPIO_AF7_USART3
#define USARTx_RX_PIN                    GPIO_PIN_11
#define USARTx_RX_GPIO_PORT              GPIOC
#define USARTx_RX_AF                     GPIO_AF7_USART3

//#define UART


void Uart_Init(void){

//	Uart_gps_config();
}

#ifdef USART
void HAL_USART_MspInit(USART_HandleTypeDef *husart)
{
  GPIO_InitTypeDef  GPIO_InitStruct;

  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO TX/RX clock */
  USARTx_TX_GPIO_CLK_ENABLE();
  USARTx_RX_GPIO_CLK_ENABLE();

  /* Enable USARTx clock */
  USARTx_CLK_ENABLE();

  /*##-2- Configure peripheral GPIO ##########################################*/
  /* UART TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = USARTx_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  GPIO_InitStruct.Speed     = GPIO_SPEED_HIGH;
  GPIO_InitStruct.Alternate = USARTx_TX_AF;

  HAL_GPIO_Init(USARTx_TX_GPIO_PORT, &GPIO_InitStruct);

  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin = USARTx_RX_PIN;
  GPIO_InitStruct.Alternate = USARTx_RX_AF;

  HAL_GPIO_Init(USARTx_RX_GPIO_PORT, &GPIO_InitStruct);
}
#endif


void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  if(huart->Instance==UART7)				// GPS.
  {
    /* Peripheral clock enable */
    __HAL_RCC_UART7_CLK_ENABLE();

    /**UART7 GPIO Configuration
    PE7     ------> UART7_RX
    PE8     ------> UART7_TX
    */
    GPIO_InitStruct.Pin 	  = GPIO_PIN_7|GPIO_PIN_8;
    GPIO_InitStruct.Mode 	  = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull 	  = GPIO_PULLUP;
    GPIO_InitStruct.Speed 	  = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_UART7;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

    //******************************** UART7 DMA Init ********************************
	/* UART7_RX Init */
	hdma_uart7_rx.Instance = DMA1_Stream3;
	hdma_uart7_rx.Init.Channel = DMA_CHANNEL_5;
	hdma_uart7_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
	hdma_uart7_rx.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_uart7_rx.Init.MemInc = DMA_MINC_ENABLE;
	hdma_uart7_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma_uart7_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	hdma_uart7_rx.Init.Mode = DMA_NORMAL;
	hdma_uart7_rx.Init.Priority = DMA_PRIORITY_HIGH;
	hdma_uart7_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
	if (HAL_DMA_Init(&hdma_uart7_rx) != HAL_OK)
	{
	  _Error_Handler(__FILE__, __LINE__);
	}

	__HAL_LINKDMA(huart,hdmarx,hdma_uart7_rx);

	/* UART7_TX Init */
	hdma_uart7_tx.Instance = DMA1_Stream1;
	hdma_uart7_tx.Init.Channel = DMA_CHANNEL_5;
	hdma_uart7_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
	hdma_uart7_tx.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_uart7_tx.Init.MemInc = DMA_MINC_ENABLE;
	hdma_uart7_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma_uart7_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	hdma_uart7_tx.Init.Mode = DMA_NORMAL;
	hdma_uart7_tx.Init.Priority = DMA_PRIORITY_LOW;
	hdma_uart7_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
	if (HAL_DMA_Init(&hdma_uart7_tx) != HAL_OK)
	{
	  _Error_Handler(__FILE__, __LINE__);
	}

	__HAL_LINKDMA(huart,hdmatx,hdma_uart7_tx);

    /* USER CODE BEGIN UART7_MspInit 1 */
    HAL_NVIC_SetPriority(UART7_IRQn, 15, 0);
    HAL_NVIC_EnableIRQ(UART7_IRQn);

  /* USER CODE END UART7_MspInit 1 */
  }

  if(huart->Instance==USART3){				//Debug.
#ifndef UART
  	  /* Peripheral clock enable */
  	  __HAL_RCC_USART3_CLK_ENABLE();

  	  /**USART3 GPIO Configuration
  	  PC10     ------> USART3_TX
  	  PC11     ------> USART3_RX
  	  */

  	  __HAL_RCC_GPIOC_CLK_ENABLE();

  	  GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11;
  	  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  	  GPIO_InitStruct.Pull = GPIO_PULLUP;
  	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  	  GPIO_InitStruct.Alternate = GPIO_AF7_USART3;
  	  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
#else
  	RCC_PeriphCLKInitTypeDef RCC_PeriphClkInit;

  	  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  	  /* Enable GPIO TX/RX clock */
  	  USARTx_TX_GPIO_CLK_ENABLE();
  	  USARTx_RX_GPIO_CLK_ENABLE();

  	  /* Select SysClk as source of USART1 clocks */
  	  RCC_PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART3;
  	  RCC_PeriphClkInit.Usart1ClockSelection = RCC_USART3CLKSOURCE_SYSCLK;
  	  HAL_RCCEx_PeriphCLKConfig(&RCC_PeriphClkInit);

  	  /* Enable USARTx clock */
  	  USARTx_CLK_ENABLE();

  	  /*##-2- Configure peripheral GPIO ##########################################*/
  	  /* UART TX GPIO pin configuration  */
  	  GPIO_InitStruct.Pin       = USARTx_TX_PIN;
  	  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  	  GPIO_InitStruct.Pull      = GPIO_PULLUP;
  	  GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
  	  GPIO_InitStruct.Alternate = USARTx_TX_AF;

  	  HAL_GPIO_Init(USARTx_TX_GPIO_PORT, &GPIO_InitStruct);

  	  /* UART RX GPIO pin configuration  */
  	  GPIO_InitStruct.Pin = USARTx_RX_PIN;
  	  GPIO_InitStruct.Alternate = USARTx_RX_AF;

  	  HAL_GPIO_Init(USARTx_RX_GPIO_PORT, &GPIO_InitStruct);
#endif
     }


}

void HAL_UART_MspDeInit(UART_HandleTypeDef* huart)
{

  if(huart->Instance==USART3)
	  {
#ifndef UART
	    /* Peripheral clock disable */
	    __HAL_RCC_USART3_CLK_DISABLE();

	    /**UART7 GPIO Configuration
	    PE10     ------> USART_TX
	    PE11     ------> USART_RX
	    */
	    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_10|GPIO_PIN_11);
#else
	    /*##-1- Reset peripherals ##################################################*/
	     USARTx_FORCE_RESET();
	     USARTx_RELEASE_RESET();

	     /*##-2- Disable peripherals and GPIO Clocks #################################*/
	     /* Configure UART Tx as alternate function  */
	     HAL_GPIO_DeInit(USARTx_TX_GPIO_PORT, USARTx_TX_PIN);
	     /* Configure UART Rx as alternate function  */
	     HAL_GPIO_DeInit(USARTx_RX_GPIO_PORT, USARTx_RX_PIN);
#endif
   }

  if(huart->Instance==UART7)
  {
    /* Peripheral clock disable */
    __HAL_RCC_UART7_CLK_DISABLE();

    /**UART7 GPIO Configuration
    PE7     ------> UART7_RX
    PE8     ------> UART7_TX
    */
    HAL_GPIO_DeInit(GPIOE, GPIO_PIN_7|GPIO_PIN_8);
  }

}


