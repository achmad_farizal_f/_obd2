/*
 * main.h
 *
 *  Created on: 11 Nov 2018
 *      Author: Achmad Farizal
 */
#ifndef MAIN_H
#define MAIN_H

#include "uart.h"
#include "uart_debug.h"
#include "config.h"
#include "gps.h"


void _Error_Handler(char * file, int line);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

#endif
