/*
 * uart.c
 *
 *  Created on: 4 Nov 2019
 *      Author: Achmad Farizal
 */

#include "uart_debug.h"
#include "string.h"

//#define UART

/* #################### External variables #################### */
static UART_HandleTypeDef 	_uart3_handle;     /**< UART3 handle */

#define UART3_BAUDRATE 115200

/* Private function prototypes -----------------------------------------------*/



void Uart_Debug_config(void)
{
	/* Configure UART3 as simple TX */
    _uart3_handle.Instance 			= USART3;
    _uart3_handle.Init.BaudRate 	= UART3_BAUDRATE;
    _uart3_handle.Init.WordLength 	= UART_WORDLENGTH_8B;
    _uart3_handle.Init.StopBits 	= UART_STOPBITS_1;
    _uart3_handle.Init.Parity 		= UART_PARITY_NONE;
    _uart3_handle.Init.Mode 		= UART_MODE_TX_RX;
    _uart3_handle.Init.HwFlowCtl 	= UART_HWCONTROL_NONE;
    _uart3_handle.Init.OverSampling = UART_OVERSAMPLING_16;

    if (HAL_UART_Init(&_uart3_handle) != HAL_OK)
    {
    	Error_Handler();
    }
}




//static uint8_t _uart3_receive(UARTPeriphCallback callback, uint32_t timeout)
//{
//
//}

//uint8_t _uart3_send(char data)
//{
//	uint8_t timeout = 0;
//	uint8_t length = 0;
//
//	timeout = 200;
//	length = 2;
//
//    return HAL_UART_Transmit(&_uart3_handle,(uint8_t *) &data, length, timeout);
//}

//HAL_StatusTypeDef debug_send(uint8_t *txData){
//
//	HAL_StatusTypeDef val;
//	HAL_UART_Transmit(&_uart3_handle, txData, 3, 1000);
//		val = HAL_UART_Transmit(&_uart3_handle, txData, 3, 1000);
//		if(!val){
//			DEBUG_PRINT("Fail");
//			return 1;
//		}
//
//		return 0;
//
//}

uint8_t debug_send(void){
	char txData[12]= "UART READY";


	HAL_StatusTypeDef val;
		val = HAL_UART_Transmit(&_uart3_handle, (uint8_t *)txData, 12, 1000);
		if(val){
			printf("Fail");
			return HAL_ERROR;
		}

		printf("successfully\n\r");
		return HAL_OK;

}

#ifdef __GNUC__
/* With GCC, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART3 and Loop until the end of transmission */
//  HAL_UART_Transmit(&UartHandle, (uint8_t *)&ch, 1, 0xFFFF);
  HAL_UART_Transmit(&_uart3_handle, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}


