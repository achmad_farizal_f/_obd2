#ifndef UARTGPS_H
#define UARTGPS_H


#include "stm32f7xx.h"
#include "stm32f7xx_hal.h"
#include "main.h"

void Uart_gps_config(void);

DMA_HandleTypeDef 		hdma_uart7_rx;
DMA_HandleTypeDef 		hdma_uart7_tx;

#endif
